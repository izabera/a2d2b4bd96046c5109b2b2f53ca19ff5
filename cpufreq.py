import collections
import itertools
import signal
import os
import time
import errno

blocks = " ▁▂▃▄▅▆▇█"


def readfreq(i, kind):
    with open(f"/sys/devices/system/cpu/cpu{i}/cpufreq/{kind}_freq") as f:
        return int(f.readline())


maxfreqs = []
freqs = []

cols, lines = 0, 0


def updatesizes(*_):
    global cols, lines, freqs
    (cols, lines) = os.get_terminal_size()
    for i, _ in enumerate(freqs):
        freqs[i] = collections.deque(freqs[i], maxlen=cols - 10)


signal.signal(signal.SIGWINCH, updatesizes)
updatesizes()

for i in itertools.count():
    try:
        # fun fact!!! did you know that your cpu can report higher current freq than this?!
        maxfreqs.append(readfreq(i, "scaling_max"))
        freqs.append(collections.deque(maxlen=cols - 10))
    except OSError as e:
        if e.errno == errno.ENOENT:
            break
        raise


def cpufreq():
    for i, _ in enumerate(maxfreqs):
        freq = readfreq(i, "scaling_cur")
        freqs[i].append(freq)
        print(
            f"cpu{i:2}",
            "".join( [ blocks[min(int(f / maxfreqs[i] * len(blocks)), len(blocks)-1)] for f in freqs[i] ]
            ),
            end="\033[K\n",
        )

    time.sleep(0.01)
    print(f"\033[{len(maxfreqs)}A", end="")


def main():
    try:
        while True:
            cpufreq()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
